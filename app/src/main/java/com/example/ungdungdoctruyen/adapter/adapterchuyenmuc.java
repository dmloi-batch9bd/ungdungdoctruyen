package com.example.ungdungdoctruyen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ungdungdoctruyen.R;
import com.example.ungdungdoctruyen.model.Truyen;
import com.example.ungdungdoctruyen.model.chuyenmuc;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class adapterchuyenmuc extends BaseAdapter {
    private Context context;
    private ArrayList<chuyenmuc> listChuyenmuc;

    public adapterchuyenmuc(Context context, ArrayList<chuyenmuc> listChuyenmuc) {
        this.context = context;
        this.listChuyenmuc = listChuyenmuc;
    }

    @Override
    public int getCount() {
        return listChuyenmuc.size();
    }

    @Override
    public Object getItem(int position) {
        return listChuyenmuc.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public void filterlist(ArrayList<chuyenmuc> filteredList) {
        listChuyenmuc = filteredList;
        notifyDataSetChanged();
    }
    public class ViewHolder{
        TextView tenchuyenmuc;
        ImageView img;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        viewHolder = new ViewHolder();

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.chuyenmuc,null);

        viewHolder.tenchuyenmuc = convertView.findViewById(R.id.textviewTenChuyenMuc);
        viewHolder.img = convertView.findViewById(R.id.imgchuyenmuc);
        convertView.setTag(viewHolder);

        chuyenmuc chuyenmuc = (chuyenmuc)getItem(position);
        viewHolder.tenchuyenmuc.setText(chuyenmuc.getTenchuyenmuc());
        Picasso.get().load(chuyenmuc.getHinhanhchuyenmuc()).placeholder(R.drawable.ic_baseline_cloud_download_24).error(R.drawable.ic_image_24).into(viewHolder.img);
        return convertView;
    }
}
