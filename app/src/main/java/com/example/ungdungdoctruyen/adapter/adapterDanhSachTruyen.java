package com.example.ungdungdoctruyen.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ungdungdoctruyen.R;
import com.example.ungdungdoctruyen.model.Truyen;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class adapterDanhSachTruyen extends BaseAdapter {
    private Context context;
    private ArrayList<Truyen> gridTruyen;

    public adapterDanhSachTruyen(Context context, ArrayList<Truyen> gridTruyen) {
        this.context = context;
        this.gridTruyen = gridTruyen;
    }

    @Override
    public int getCount() {
        return gridTruyen.size();
    }

    @Override
    public Object getItem(int position) {
        return gridTruyen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public class ViewHolder{
        ImageView imageView;
        TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        viewHolder = new ViewHolder();
        LayoutInflater inflater2 = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater2.inflate(R.layout.mucgirdview,null);
        viewHolder.imageView = convertView.findViewById(R.id.imgTruyenDS);
        viewHolder.textView = convertView.findViewById(R.id.textviewTenTruyenDS);
        convertView.setTag(viewHolder);
        Truyen truyen = (Truyen) getItem(position);
        viewHolder.textView.setText(truyen.getTenTruyen());
        Picasso.get().load(truyen.getAnh()).placeholder(R.drawable.ic_baseline_cloud_download_24).error(R.drawable.ic_image_24).into(viewHolder.imageView);
        return convertView;
    }
}
