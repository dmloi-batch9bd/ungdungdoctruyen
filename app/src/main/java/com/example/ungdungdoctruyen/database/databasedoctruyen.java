package com.example.ungdungdoctruyen.database;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import com.example.ungdungdoctruyen.R;
import com.example.ungdungdoctruyen.model.TaiKhoan;
import com.example.ungdungdoctruyen.model.Truyen;

public class databasedoctruyen extends SQLiteOpenHelper {
    //cơ sở dữ liệu
    //tên database
    private static String DATABASE_NAME = "doctruyen";
    //biến bảng tài khoản
    private static String TABLE_TAIKHOAN = "taikhoan";
    private static String ID_TAI_KHOAN = "idtaikhoan";
    private static String TEN_TAI_KHOAN = "tentaikhoan";
    private static String MAT_KHAU = "matkhau";
    private static String PHAN_QUYEN = "phanquyen";
    private static String EMAIL = "email";
    // version
    private static int VERSION = 1;
    // biến bảng truyện
    private static String TABLE_TRUYEN = "truyen";
    private static String ID_TRUYEN = "idtruyen";
    private static String TEN_TRUYEN = "tieude";
    private static String NOI_DUNG = "noidung";
    private static String IMAGE = "anh";

    // context
    private Context context;
    //Biến luu câu lệnh tạo bảng tài khoản
    private String SQLQuery = "CREATE TABLE "+ TABLE_TAIKHOAN +" ( "+ID_TAI_KHOAN+" INTEGER PRIMARY KEY AUTOINCREMENT, "
            +TEN_TAI_KHOAN+" TEXT UNIQUE, "
            +MAT_KHAU+" TEXT, "
            +EMAIL+" TEXT, "
            + PHAN_QUYEN+" INTEGER) ";
    //Biến lưu câu lệnh tạo bảng truyện
    private String SQLQuery1 = "CREATE TABLE "+ TABLE_TRUYEN +" ( "+ID_TRUYEN+" integer primary key AUTOINCREMENT, "
            +TEN_TRUYEN+" TEXT UNIQUE, "
            +NOI_DUNG+" TEXT, "
            +IMAGE+" TEXT, "+ID_TAI_KHOAN+" INTEGER , FOREIGN KEY ( "+ ID_TAI_KHOAN +" ) REFERENCES "+
            TABLE_TAIKHOAN+"("+ID_TAI_KHOAN+"))";
    //Insert dữ liệu vào bảng
    //Phân quyền 1: admin ; 2: người dùng
    private String SQLQuery2 = "INSERT INTO TaiKhoan VAlUES (null,'admin','admin','admin@gmail.com',2)";
    private String SQLQuery3 = "INSERT INTO TaiKhoan VAlUES (null,'loiduong','123','loiduong@gmail.com',1)";
    private String SQLQuery9 = "INSERT INTO TaiKhoan VAlUES (null,'trieudang','123','trieudang@gmail.com',1)";
    //insert truyện
    private String SQLQuery4 = "INSERT INTO truyen VALUES (null,'Dưới Mái Ngói Đơn Sơ','Phần 1:\n" +
            "\n" +
            "Còn gì vất vả hơn lặn lội đường xa trong trời đông giá rét.\n" +
            "Tê Trì ngồi trong xe ngựa, than trong chậu vẫn cháy đỏ, ấy vậy mà chẳng cảm nhận được chút hơi ấm, thậm chí dù đã kéo kín rèm xe thì vẫn có gió lạnh lùa vào.\n" +
            "Nàng khép tay áo lại, tay lồng vào bên trong, đợi chà xát sinh nhiệt rồi mới giơ hai ngón tay rca, vén rèm lên nhìn ra ngoài.\n" +
            "Trận tuyết nặng hạt hôm nay vừa ngớt, tuyết đọng vẫn chưa tan, một vùng trắng xóa bao phủ tầm mắt.\n" +
            "Dọc đường cây cối xơ xác khẳng khiu, đụn tuyết trắng nằm trên cành mục lá khô, gió thổi qua làm lá cây đung đưa, cũng làm bông tuyết rơi xuống lã chã.\n" +
            "Đây là thuộc địa Bắc quốc, thua xa những nơi nàng dừng chân trước đó, trong ấn tượng của mình, nàng chưa từng thấy tuyết dày như vậy bao giờ.\n" +
            "Kéo xe cho nàng đều là ngựa cao to đến từ Tây Vực, thế mà vó hạ xuống, tuyết cũng ngập đến nửa cẳng chân, đi lại vô cùng khó khăn.\n"+
            "Bỗng tấm rèm dày khẽ động, ngay tiếp đó có tiếng người vang lên, thị nữ lúc nãy ra ngoài nhìn đường cách rèm thấp giọng gọi: “Gia chủ đã ngủ chưa ạ?”\n"+
            "Tê Trì nhìn ra ngoài xe: “Chưa, có chuyện gì cứ nói đi.”\n"+
            " “Là thế tử…” Tân Lộ dừng lại, sau đó nói: “Thế tử sai nô tỳ đến truyền lời, nói muốn ngồi cùng xe với người.”\n"+
            "Tê Trì ngoái đầu nhìn chiếc xe ở phía sau rồi buông rèm xuống, không nói năng gì.\n"+
            "Người ngồi trong cỗ xe đằng sau là cháu trai nàng – Quang vương thế tử Lý Nghiên.\n"+
            "Tân Lộ ở ngoài cửa dựng lỗ tai lên, nhưng chờ mãi mà vẫn không chờ được câu trả lời.\n"+
            "Nàng là nô tỳ thiếp thân nên biết rõ mọi chuyện, thế tử còn nhỏ mà đã không cha không mẹ, một thân một mình được gia chủ nuôi nấng.\n"+
            "Trước đây gia chủ rất quan tâm đến cậu ấy, cưng chiều như bảo bối, thế mà lần này đường xá vất vả, gia chủ lại để cậu ngồi một mình một xe. Tân Lộ nghĩ, có lẽ là vì chuyện trước đó.\n"+
            "Thời gian trước tan học đi về, thế tử mang theo vết thương về nhà, kinh động trên dưới toàn phủ, nghe nói là sinh sự với người ta.\n"+
            "Rồi bỗng sau đó gia chủ hạ lệnh chuyển nhà, chuẩn bị qua loa hành trang đơn giản, ngàn dặm xa xôi đến Bắc quốc mênh mông, không biết có phải muốn noi theo mẹ Mạnh Tử chuyển nhà ba lần* không…\n"+
            "(*Mẹ của Mạnh Tử dọn nhà ba lần để chọn láng giềng tốt cho ông. Lần thứ nhất, nhà ở gần nghĩa địa. Lần thứ hai, nhà ở gần chợ mua bán. Cuối cùng bà dọn nhà một lần nữa để ở gần trường học, cho Mạnh Tử hoàn cảnh tốt về giáo dục.)\n"+
            "Vừa nghĩ đến đó, bất chợt nghe thấy Tê Trì trong xe lên tiếng: “Thằng bé còn đang bị thương, bảo nó ngồi một chỗ cho yên ổn, đừng lộn xộn nữa.”\n"+
            "Tức là không cho phép.\n"+
            "Tân Lộ thở dài đáp “vâng” rồi nghĩ xem nên trả lời thế tử thế nào. Cậu đâu biết dọc đường đi nàng đã phải nói bao nhiêu lời hay, vừa rồi vất vả lắm mới tìm được cơ hội mở miệng, nhưng sự vẫn không thành.\n"+
            "Một chốc sau, Tê Trì hỏi: “Còn xa nữa không?”\n"+
            "Tân Lộ đáp: “Còn chưa đến mười dặm nữa ạ.” Rồi không nói gì thêm.\n"+
            "Nhất thời bầu không khí trở nên yên ắng, chỉ còn lại tiếng bánh xe lọc cọc cán qua tuyết dày.\n"+
            "Tê Trì ngồi thẳng lưng, kỳ thực trong lòng cũng đang nhớ cháu.\n"+
            "Đó là một đứa bé đáng thương, là độc đinh của Quang vương – ca ca của nàng.\n"+
            "Ngày trước khi lâm bồn, tẩu tẩu của nàng là Quang vương phi đã qua đời vì khó sinh, thậm chí còn chưa kịp nhìn con đã nhắm mắt xuôi tay.\n"+
            "Còn nữa!','https://img.truyentrz.com/2020/11/duoi-mai-ngoi-don-so-215x322.jpg?1',1)";
    private String SQLQuery5 = "INSERT INTO truyen VALUES (null,'Truyện cổ tích chú cuội','Ngày xưa ở một miền nọ có một người tiều phu tên là Cuội. Một hôm, như lệ thường, Cuội vác rìu vào rừng sâu tìm cây mà chặt. Khi đến gần một con suối nhỏ, Cuội bỗng giật mình trông thấy một cái hang cọp. Nhìn trước nhìn sau anh chỉ thấy có bốn con cọp con đang vờn nhau. Cuội liền xông đến vung rìu bổ cho mỗi con một nhát lăn quay trên mặt đất. Nhưng vừa lúc đó, cọp mẹ cũng về tới nơi. Nghe tiếng gầm kinh hồn ở sau lưng, Cuội chỉ kịp quẳng rìu leo thoắt lên ngọn một cây cao.\n" +
            "Từ trên nhìn xuống, Cuội thấy cọp mẹ lồng lộn trước đàn con đã chết. Nhưng chỉ một lát, cọp mẹ lẳng lặng đi đến một gốc cây gần chỗ Cuội ẩn, đớp lấy một ít lá rồi trở về nhai và mớm cho con. Chưa đầy ăn giập miếng trầu, bốn con cọp con đã vẫy đuôi sống lại, khiến cho Cuội vô cùng sửng sốt. Chờ cho cọp mẹ tha con đi nơi khác, Cuội mới lần xuống tìm đến cây lạ kia đào gốc vác về.\n" +
            "Dọc đường gặp một ông lão ăn mày nằm chết vật trên bãi cỏ, Cuội liền đặt gánh xuống, không ngần ngại, bứt ngay mấy lá nhai và mớm cho ông già! Mầu nhiệm làm sao, mớm vừa xong, ông lão đã mở mắt ngồi dậy. Thấy có cây lạ, ông lão liền hỏi chuyện. Cuội thực tình kể lại đầu đuôi. Nghe xong ông lão kêu lên:\n" +
            "– Trời ơi! Cây này chính là cây có phép “cải tử hoàn sinh” đây. Thật là trời cho con để cứu giúp thiên hạ. Con hãy chăm sóc cho cây nhưng nhớ đừng tưới bằng nước bẩn mà cây bay lên trời đó!\n" +
            "Nói rồi ông lão chống gậy đi. Còn Cuội thì gánh cây về nhà trồng ở góc vườn phía đông, luôn luôn nhớ lời ông lão dặn, ngày nào cũng tưới bằng nước giếng trong.\n" +
            "Từ ngày có cây thuốc quý, Cuội cứu sống được rất nhiều người. Hễ nghe nói có ai nhắm mắt tắt hơi là Cuội vui lòng mang lá cây đến tận nơi cứu chữa. Tiếng đồn Cuội có phép lạ lan đi khắp nơi.\n" +
            "Một hôm, Cuội lội qua sông gặp xác một con chó chết trôi. Cuội vớt lên rồi giở lá trong mình ra cứu chữa cho chó sống lại. Con chó quấn quít theo Cuội, tỏ lòng biết ơn. Từ đấy, Cuội có thêm một con vật tinh khôn làm bạn.\n" +
            "Một lần khác, có lão nhà giàu ở làng bên hớt hải chạy đến tìm Cuội, vật nài xin Cuội cứu cho con gái mình vừa sẩy chân chết đuối. Cuội vui lòng theo về nhà, lấy lá chữa cho. Chỉ một lát sau, mặt cô gái đang tái nhợt bỗng hồng hào hẳn lên, rồi sống lại. Thấy Cuội là người cứu sống mình, cô gái xin làm vợ chàng. Lão nhà giàu cũng vui lòng gả con cho Cuội.\n" +
            "Vợ chồng Cuội sống với nhau thuận hòa, êm ấm thì thốt nhiên một hôm, trong khi Cuội đi vắng, có bọn giặc đi qua nhà Cuội. Biết Cuội có phép cải tử hoàn sinh, chúng quyết tâm chơi ác. Chúng bèn giết vợ Cuội, cố ý moi ruột người đàn bà vứt xuống sông, rồi mới kéo nhau đi. Khi Cuội trở về thì vợ đã chết từ bao giờ, mớm bao nhiêu lá vẫn không công hiệu, vì không có ruột thì làm sao mà sống được.\n" +
            "Thấy chủ khóc thảm thiết, con chó lại gần xin hiến ruột mình thay vào ruột vợ chủ. Cuội chưa từng làm thế bao giờ, nhưng cũng liều mượn ruột chó thay ruột người xem sao. Quả nhiên người vợ sống lại và vẫn trẻ đẹp như xưa. Thương con chó có nghĩa, Cuội bèn nặn thử một bộ ruột bằng đất, rồi đặt vào bụng chó, chó cũng sống lại. Vợ với chồng, người với vật lại càng quấn quít với nhau hơn xưa.\n" +
            "Nhưng cũng từ đấy, tính nết vợ Cuội tự nhiên thay đổi hẳn. Hễ nói đâu là quên đó, làm cho Cuội lắm lúc bực mình. Ðã không biết mấy lần, chồng dặn vợ: “Có đái thì đái bên Tây, chớ đái bên Ðông, cây dông lên trời!”. Nhưng vợ Cuội hình như lú ruột, lú gan, vừa nghe dặn xong đã quên biến ngay.\n" +
            "Một buổi chiều, chồng còn đi rừng kiếm củi chưa về, vợ ra vườn sau, không còn nhớ lời chồng dặn, cứ nhằm vào gốc cây quý mà đái. Không ngờ chị ta vừa đái xong thì mặt đất chuyển động, cây đảo mạnh, gió thổi ào ào. Cây đa tự nhiên bật gốc, lững thững bay lên trời.\n" +
            "Vừa lúc đó thì Cuội về đến nhà. Thấy thế, Cuội hốt hoảng vứt gánh củi, nhảy bổ đến, toan níu cây lại. Nhưng cây lúc ấy đã rời khỏi mặt đất lên quá đầu người. Cuội chỉ kịp móc rìu vào rễ cây, định lôi cây xuống, nhưng cây vẫn cứ bốc lên, không một sức nào cản nổi. Cuội cũng nhất định không chịu buông, thành thử cây kéo cả Cuội bay vút lên đến cung trăng.\n" +
            " Từ đấy Cuội ở luôn cung trăng với cả cái cây quý của mình. Mỗi năm cây chỉ rụng xuống biển có một lá. Bọn cá heo đã chực sẵn, khi lá xuống đến mặt nước là chúng tranh nhau đớp lấy, coi như món thuốc quý để cứu chữa cho tộc loại chúng. Nhìn lên mặt trăng, người ta thấy một vết đen rõ hình một cây cổ thụ có người ngồi dưới gốc, người ta gọi cái hình ấy là hình chú Cuội ngồi gốc cây đa….','https://salt.tikicdn.com/ts/product/2d/fc/34/4c544b0625b52f2a7bec03ec2eb7b234.jpg',1)";
    private String SQLQuery6 = "INSERT INTO truyen VALUES (null,'Cô bé quàng khăn đỏ','Ngày xửa, ngày xưa, có một cô bé thường hay quàng chiếc khăn màu đỏ, vì vậy, mọi người gọi cô là cô bé quàng khăn đỏ. Một hôm, mẹ cô bảo cô mang bánh sang biếu bà ngoại. Trước khi đi, mẹ cô dặn: \n" +
            "– Con đi thì đi đường thẳng, đừng đi đường vòng qua rừng mà chó sói ăn thịt con đấy. Trên đường đi, cô thấy đường vòng qua rừng có nhiều hoa, nhiều bướm, không nghe lời mẹ dặn, cô tung tăng đi theo đường đó. Đi được một quãng thì gặp Sóc, Sóc nhắc:\n" +
            "– Cô bé quàng khăn đỏ ơi, lúc nãy tôi nghe mẹ cô dặn đi đường thẳng, đừng đi đường vòng cơ mà. Sao cô lại đi đường này?\n" +
            "Cô bé không trả lời Sóc. Cô cứ đi theo đường vòng qua rừng. Vừa đi, cô vừa hái hoa, bắt bướm. Vào đến cửa rừng thì cô gặp chó sói. Con chó sói rất to đến trước mặt cô. Nó cất giọng ồm ồm hỏi:\n" +
            "– Này, cô bé đi đâu thế?\n" +
            "Nghe chó sói hỏi, cô bé quàng khăn đỏ sợ lắm, nhưng cũng đành bạo dạn trả lời:\n" +
            "– Tôi đi sang nhà bà ngoại tôi.\n" +
            "Nghe cô bé nói đi sang bà ngoại, chó sói nghĩ bụng: À, thì ra nó lại còn có bà ngoại nữa, thế thì mình phải ăn thịt cả hai bà cháu. Nghĩ vậy nên chó sói lại hỏi:\n" +
            "– Nhà bà ngoại cô ở đâu?\n" +
            "– Ở bên kia khu rừng. Cái nhà có ống khói đấy, cứ đẩy cửa là vào được ngay.\n" +
            "Nghe xong, chó sói bỏ cô bé quàng khăn đỏ ở đấy rồi chạy một mạch đến nhà bà ngoại cô bé. Nó đẩy cửa vào vồ lấy bà cụ rồi nuốt chửng ngay vào bụng. Xong xuôi, nó lên giường nằm đắp chăn giả là bà ngoại ốm.\n" +
            "Lúc cô bé quàng khăn đỏ đến, cô thấy chó sói đắp chăn nằm trên giường, cô tưởng “bà ngoại” bị ốm thật, cô hỏi:\n" +
            "– Bà ơi! Bà ốm đã lâu chưa?\n" +
            "Sói không đáp giả vờ rên hừ… hừ…\n" +
            "– Bà ơi, mẹ cháu bảo mang bánh sang biếu bà.\n" +
            "– Thế à, thế thì bà cám ơn cháu và mẹ cháu. Cháu ngoan quá. Cháu lại đây với bà.\n" +
            "Cô bé quàng khăn đỏ chạy ngay đến cạnh giường, nhưng cô ngạc nhiên lùi lại hỏi;\n" +
            "– Bà ơi! Sao hôm nay tai bà dài thế?\n" +
            "– Tai bà dài để bà nghe cháu nói được rõ hơn. Chó sói đáp\n"+
            "– Thế còn mắt bà, sao hôm nay mắt bà to thế?\n"+
            "– Mắt bà to để bà nhìn cháu được rõ hơn.\n"+
            "Chưa tin, cô bé quàng khăn đỏ lại hỏi:\n"+
            "– Thế còn mồm bà, sao hôm nay mồm bà to thế?\n"+
            "– Mồm bà to để bà ăn thịt cháu đấy.\n"+
            "Sói nói xong liền nhảy ra khỏi giường, nuốt chửng em bé Khăn Đỏ đáng thương.\n"+
            "Sói đã no nê lại nằm xuống giường ngủ ngáy o o. May sao, lúc đó bác thợ săn đi ngang thấy thế. Bác giơ súng lên định bắn. Nhưng bác chợt nghĩ ra là chắc sói đã ăn thịt bà lão, và tuy vậy vẫn còn có cơ cứu bà. Bác nghĩ không nên bắn mà nên lấy kéo rạch bụng con sói đang ngủ ra. Vừa rạch được vài mũi thì thấy chiếc khăn quàng đỏ chóe, rạch được vài mũi nữa thì cô bé nhảy ra kêu:\n"+
            "– Trời ơi! Cháu sợ quá! Trong bụng sói, tối đen như mực. Bà lão cũng còn sống chui ra, thở hổn hển. Khăn đỏ vội đi nhặt đá to nhét đầy bụng sói. Sói tỉnh giấc muốn nhảy lên, nhưng đá nặng quá, nó ngã khuỵu xuống, lăn ra chết.\n"+
            " Từ dạo ấy, cô bé quàng khăn đỏ không bao giờ dám làm sai lời mẹ dặn.','https://product.hstatic.net/1000186499/product/bia_-_co_be_quang_khan_do_6-2020_9e700aae234c4814a8a7b687059e1e1c_master.jpg',1)";
    private String SQLQuery7 = "INSERT INTO truyen VALUES (null,'Sự tích dưa hấu','Ngày xưa, Vua Hùng Vương thứ 18 có nuôi một đứa trẻ thông minh khôi ngô, đặt tên là Mai Yển, hiệu là An Tiêm.\n" +
            "Lớn lên, vua cưới vợ cho An Tiêm, và tin dùng ở triều đình. Cậy nhờ ơn Vua cha, nhưng An Tiêm lại kiêu căng cho rằng tự sức mình tài giỏi mới gây dựng được sự nghiệp, chứ chẳng nhờ ai. Lời nói này đến tai vua, vua cho An Tiêm là kẻ kiêu bạc vô ơn, bèn đày An Tiêm cùng vợ con ra một hòn đảo xa, ở ngoài biển Nga Sơn (Thanh Hoá, Bắc Việt).\n" +
            "Người vợ là nàng Ba lo sợ sẽ phải chết ở ngoài cù lao cô quạnh, nhưng An Tiêm thì bình thản nói: “Trời đã sinh ra ta, sống chết là ở Trời và ở ta, việc gì phải lo”.\n" +
           "Hai vợ chồng An Tiêm cùng đứa con đã sống hiu quạnh ở một bãi cát, trên hoang đảo. Họ ra sức khai khẩn, trồng trọt để kiếm sống. Một ngày kia, vào mùa hạ, có một con chim lạ từ phương tây bay đến đậu trên một gò cát. Chim nhả mấy hạt gì xuống đất. Được ít lâu, thì hạt nẩy mầm, mọc dây lá cây lan rộng.\n"+
            "Cây nở hoa, kết thành trái to. Rất nhiều trái vỏ xanh, ruột đỏ. An Tiêm bảo vợ: “Giống cây này tự nhiên không trồng mà có tức là vật của Trời nuôi ta đó”. Rồi An Tiêm hái nếm thử, thấy vỏ xanh, ruột đỏ, hột đen, mùi vị thơm và ngon ngọt, mát dịu. An Tiêm bèn lấy hột gieo trồng khắp nơi, sau đó mọc lan ra rất nhiều.\n"+
            "Một ngày kia, có một chiếc tàu bị bão dạt vào cù lao. Mọi người lên bãi cát, thấy có nhiều quả lạ, ngon. Họ đua nhau đổi thực phẩm cho gia đình An Tiêm. Rồi từ đó, tiếng đồn đi là có một giống dưa rất ngon ở trên đảo. Các tàu buôn tấp nập ghé đến đổi chác đủ thứ vật dụng và thực phẩm cho gia đình An Tiêm. Nhờ đó mà gia đình bé nhỏ của An Tiêm trở nên đầy đủ, cuộc sống phong lưu.\n"+
            "Vì chim đã mang hột dưa đến từ phương Tây, nên An Tiêm đặt tên cho thứ trái cây này là Tây Qua. Người Tàu ăn thấy ngon, khen là “hẩu”, nên về sau người ta gọi trại đi là Dưa Hấu.\n"+
            "Ít lâu sau, vua sai người ra cù lao ngoài biển Nga Sơn dò xét xem gia đình An Tiêm ra làm sao, sống hay chết. Sứ thần về kể lại cảnh sống sung túc và nhàn nhã của vợ chồng An Tiêm, nhà vua ngẫm nghĩ thấy thầm phục đứa con nuôi, bèn cho triệu An Tiêm về phục lại chức vị cũ trong triều đình.\n"+
            "\n"+
            "An Tiêm đem về dâng cho vua giống dưa hấu mà mình may mắn có được. Rồi phân phát hột dưa cho dân chúng trồng ở những chỗ đất cát, làm giàu thêm cho xứ Việt một thứ trái cây danh tiếng. Hòn đảo mà An Tiêm ở, được gọi là Châu An Tiêm.','https://vn-test-11.slatic.net/original/94814fa5320ba6fefdc1ab3d149ec0c2.jpg_720x720q80.jpg_.webp',1)";
    private String SQLQuery8 = "INSERT INTO truyen VALUES (null,'Truyện cổ tích Tấm Cám','Ngày xửa ngày xưa, có hai chị em cùng cha khác mẹ, chị tên là Tấm, em tên là Cám. Mẹ Tấm mất sớm, sau đó mấy năm cha Tấm cũng qua đời, Tấm ở với dì ghẻ là mẹ Cám. Bà mẹ kế này rất cay nghiệt, bắt Tấm phải làm hết mọi việc nặng nhọc từ việc nhà đến việc chăn trâu cắt cỏ. Trong khi đó Cám được nuông chiều không phải làm gì cả.\n" +
            "Một hôm bà ta cho hai chị em mỗi người một cái giỏ bảo ra đồng xúc tép, còn hứa \"Hễ đứa nào bắt được đầy giỏ thì thưởng cho một cái yếm đỏ\". Ra đồng, Tấm chăm chỉ bắt được đầy giỏ, còn Cám thì mải chơi nên chẳng bắt được gì.\n" +
            "Thấy Tấm bắt được một giỏ đầy, Cám bảo chị :\n" +
            "- Chị Tấm ơi, chị Tấm! Đầu chị lấm, chị hụp cho sâu, kẻo về mẹ mắng.\n" +
            "Tin là thật, Tấm bèn xuống ao lội ra chỗ sâu tắm rửa. Cám thừa dịp trút hết tép của Tấm vào giỏ của mình rồi ba chân bốn cẳng về trước. Lúc Tấm bước lên chỉ còn giỏ không, bèn ngồi xuống bưng mặt khóc hu hu. Nghe tiếng khóc của Tấm, Bụt liền hiện lên hỏi :\n" +
            "- Làm sao con khóc ?\n" +
            "Tấm kể lể sự tình cho Bụt nghe, Bụt bảo:\n" +
            "- Thôi con hãy nín đi ! Con thử nhìn vào giỏ xem còn có gì nữa không?\n" +
            "Tấm nhìn vào giỏ rồi nói :  - Chỉ còn một con cá bống.\n" +
            "- Con đem con cá bống ấy về thả xuống giếng mà nuôi. Mỗi bữa, đáng ăn ba bát thì con ăn hai còn một đem thả xuống cho bống. Mỗi lần cho ăn con nhớ gọi như thế này:\n" +
            "Bống bống bang bang \n" +
            "\n" +
            "Lên ăn cơm vàng cơm bạc nhà ta\n" +
            "\n" +
            "Chớ ăn cơm hẩm cháo hoa nhà người.\n" +
            "Không gọi đúng như thế thì nó không lên, con nhớ lấy !\n" +
            "Nói xong Bụt biến mất. Tấm theo lời Bụt thả bống xuống giếng. Rồi từ hôm ấy trở đi, cứ mỗi bữa ăn, Tấm đều để dành cơm, giấu đưa ra cho bống. Mỗi lần nghe Tấm gọi, bống lại ngoi lên mặt nước đớp những hạt cơm của Tấm ném xuống. Người và cá ngày một quen nhau, và bống ngày càng lớn lên trông thấy.\n" +
            "Thấy Tấm sau mỗi bữa ăn thường mang cơm ra giếng, mụ dì ghẻ sinh nghi, bèn bảo Cám đi rình. Cám nấp ở bụi cây bên bờ giếng nghe Tấm gọi bống, bèn nhẩm lấy cho thuộc rồi về kể lại cho mẹ nghe. Tối hôm ấy mụ dì ghẻ lấy giọng ngọt ngào bảo với Tấm: \n" +
            "- Con ơi con! Làng đã bắt đầu cấm đồng rồi đấy. Mai con đi chăn trâu, phải chăn đồng xa, chớ chăn đồng nhà, làng bắt mất trâu.\n" +
            "Tấm vâng lời, sáng hôm sau đưa trâu đi ăn thật xa. Ở nhà, mẹ con Cám mang bát cơm ra giếng cũng gọi bống lên ăn y như Tấm gọi. Nghe lời gọi, bống ngoi lên mặt nước. Mẹ Cám đã chực sẵn, bắt lấy bống đem về nhà làm thịt.\n" +
            "Đến chiều Tấm dắt trâu về, sau khi ăn xong Tấm lại mang bát cơm để dành ra giếng, Tấm gọi nhưng chả thấy bống ngoi lên như mọi khi. Tấm gọi mãi, gọi mãi, cuối cùng chỉ thấy cục máu nổi lên mặt nước. Biết là có sự chẳng lành cho bống, Tấm òa lên khóc. Bụt lại hiện lên hỏi: \n" +
            "- Con làm sao lại khóc ?\n" +
            "Tấm kể sự tình cho Bụt nghe, Bụt bảo:\n" +
            "- Con bống của con, người ta đã ăn thịt mất rồi. Thôi con hãy nín đi ! Rồi về nhặt xương nó, kiếm bốn cái lọ bỏ vào, đem chôn xuống dưới bốn chân giường con nằm.\n" +
            "Tấm trở về theo lời Bụt đi tìm xương bống, nhưng tìm mãi các xó vườn góc sân mà không thấy đâu cả. Một con gà thấy thế, bảo Tấm :\n" +
            "- Cục ta cục tác ! Cho ta nắm thóc, ta bưới xương cho !\n" +
            "Tấm bốc nắm thóc ném cho gà. Gà chạy vào bếp bới một lúc thì thấy xương ngay. Tấm bèn nhặt lấy bỏ vào lọ và đem chôn dưới chân giường như lời bụt dặn.\n" +
            "Ít lâu sau nhà vua mở hội trong mấy đêm ngày. Già trẻ gái trai các làng đều nô nức đi xem, trên các nẻo đường, quần áo mớ ba mớ bẩy dập dìu tuôn về kinh như nước chảy. Hai mẹ con Cám cũng sắm sửa quần áo đẹp để đi trẩy hội. Thấy Tấm cũng muốn đi, mụ dì ghẻ nguýt dài, sau đó mụ lấy một đấu gạo trộn lẫn với một đấu thóc, bảo Tấm: \n" +
            "- Khi nào nhặt riêng gạo và thóc ra hai đấu thì mới được đi xem hội. \n" +
            "Nói đoạn, hai mẹ con quần áo xúng xính lên đường. Tấm tủi thân òa lên khóc. Bụt lại hiện lên hỏi:\n" +
            "- Làm sao con khóc?\n" +
            "\n" +
            "Tấm chỉ vào cái thúng, thưa:\n" +
            "\n" +
            "- Dì con bắt phải nhặt thóc ra thóc, gạo ra gạo, rồi mới được đi xem hội, lúc nhặt xong thì hội đã tan rồi còn gì mà xem.\n" +
            "\n" +
            "Bụt bảo: - Con đừng khóc nữa. Con mang cái thúng đặt ra giữa sân, để ta sai chim sẻ xuống nhặt giúp. \n" +
            "\n" +
            "- Nhưng ngộ nhỡ chim sẻ ăn mất thì khi về con vẫn cứ bị đòn.\n" +
            "\n" +
            "- Con cứ bảo chúng nó thế này:\n" +
            "\n" +
            "Rặt rặt (con chim sẻ) xuống nhặt cho tao\n" +
            "\n" +
            "Ăn mất hạt nào thì tao đánh chết\n" +
            "Thì chúng nó sẽ không ăn của con đâu.\n" +
            "Bụt vừa dứt lời, ở trên không có một đàn chim sẻ đáp xuống sân nhặt thóc ra một đằng, gạo ra một nẻo. Chúng nó lăng xăng ríu rít chỉ trong một lát đã làm xong, không suy suyển một hạt. Nhưng khi chim sẻ bay đi rồi, Tấm lại nức nở khóc. Bụt lại bảo: \n" +
            "\n" +
            "- Con làm sao lại khóc?\n" +
            "- Con rách rưới quá, người ta không cho con vào xem hội.\n" +
            "- Con hãy đào những cái lọ xương bống đã chôn ngày trước lên thì sẽ có đủ thứ cho con trẩy hội.\n" +
            "Tấm vâng lời, đi đào các lọ lên. Đào lọ thứ nhất lấy ra được một cái áo mớ ba, một cái áo xống lụa, một cái yếm lụa điều và một cái khăn nhiễu. Đào lọ thứ hai lấy ra được một đôi hài thêu. Đào lọ thứ ba thì thấy một con ngựa bé tí, nhưng vừa đặt con ngựa xuông đất bỗng chốc nó đã hí vang lên và biến thành ngựa thật. Đào đến lọ cuối cùng thì lấy ra được một bộ yên cương xinh xắn.\n" +
            "\n" +
            "Tấm mừng quá vội tắm rửa rồi thắng bộ vào, đoạn cưỡi lên ngựa mà đi. Ngựa phóng một chốc đã đến kinh đô. Nhưng khi phóng qua một cây cầu đá, Tấm đánh rơi một chiếc hài xuống nước, không cách nào mò lên được. \n" +
            "Khi đoàn xa giá chở vua đi qua cầu, con voi ngự bỗng nhiên cắm ngà xuống đất kêu rống lên, không chịu đi. Vua sai quân lính xuống nước thử tìm xem, họ mò được một chiếc hài thêu rất tinh xảo và xinh đẹp. Vua ngắm nghía chiếc hài hồi lâu rồi hạ lệnh cho rao mời tất cả đám đàn bà con gái xem hội ướm thử, hễ ai đi vừa chiếc giầy thì vua sẽ lấy làm vợ.\n"+
            "Đám hội lại càng náo nhiệt vì các bà, các cô chen nhau đến chỗ thử giầy. Cô nào cô ấy lần lượt kéo vào ngôi lầu giữa bãi cỏ rộng để ướm một tí cầu may. Nhưng chẳng có một chân nào đi vừa cả. Mẹ con Cám cũng trong số đó. Khi Cám và dì ghẻ bước ra khỏi lầu thì gặp Tấm, Cám mách mẹ:\n" +
            " - Mẹ ơi, ai như chị Tấm cũng đi thử hài đấy!\n" +
            "Mụ dì ghẻ bĩu môi:\n"+
            "- Chuông khánh còn chẳng ăn ai, nữa là mảnh chĩnh vứt ngoài bờ tre!\n"+
            "Nhưng khi Tấm đặt chân vào hài thì vừa như in. Nàng mở khăn lấy luôn chiếc thứ hai đi vào. Hai chiếc hài giống nhau như đúc. Bọn lính hầu hò reo vui mừng. Lập tức vua sai đoàn tỳ nữ rước nàng vào cung. Tấm bước lên kiệu trước con mắt ngạc nhiên và hằn học của mẹ con Cám.\n"+
            "Tuy sống sung sướng trong hoàng cung. Tấm vẫn không quên ngày giỗ cha. Nàng xin phép vua trở về nhà để soạn cỗ cúng giúp dì. Mẹ con Cám thấy Tấm sung sướng thì ghen ghét để bụng. Nay thấy Tấm về, lòng ghen ghét lại bùng bốc lên. Nghĩ ra được một mưu, mụ dì ghẻ bảo Tấm:\n"+
            "- Trước đây con quen trèo cau, con hãy trèo lên xé lấy một buồng cau để cúng bố. \n"+
            "Tấm vâng lời trèo lên cây cau, lúc lên đến sát buồng thì ở dưới này mụ dì ghẻ cầm dao đẵn gốc. Thấy cây rung chuyển, Tấm hỏi :\n"+
            "- Dì làm gì dưới gốc thế ?\n"+
            "- Gốc cau lắm kiến, dì đuổi kiến cho nó khỏi lên đốt con.\n"+
            "Nhưng Tấm chưa kịp xé cau thì cây cau đã đổ. Tấm ngã lộn cổ xuống ao chết. Mụ dì ghẻ vội vàng lột áo quần của Tấm cho con mình mặc vào cung nói dối với vua rằng Tấm không may bị rơi xuống ao chết đuối, nay đưa em vào để thế chị. Vua nghe nói trong bụng không vui, nhưng không biết phải làm thế nào cả.\n"+
            "Lại nói chuyện Tấm chết hóa thành chim Vàng anh, chim bay một mạch về kinh đến vườn ngự. Thấy Cám đang giặt áo cho vua ở giếng, Vàng anh dừng lại trên cành cây, bảo nó:\n" +
            "- Phơi áo chồng tao, phơi lao phơi sào, chớ phơi bờ rào, rách áo chồng tao.\n" +
            "Rồi chim Vàng anh bay thẳng vào cung rồi đậu ở cửa sổ, hót lên rất vui tai. Vua đi đâu, chim bay đến đó. Vua đang nhớ Tấm không nguôi, thấy chim quyến luyến theo mình, vua bảo:\n" +
            "- Vàng ảnh vàng anh, có phải vợ anh, chui vào tay áo.\n" +
            "Chim vàng anh bay lại đậu vào tay vua rồi rúc vào tay áo. Vua yêu quý vàng anh quên cả ăn ngủ. Vua sai làm một cái lồng bằng vàng cho chim ở. Từ đó, ngày đêm vua chỉ mải mê với chim, không tưởng đến Cám. \n" +
            "Cám vội về mách mẹ. Mẹ nó bảo cứ bắt chim làm thịt ăn rồi kiếm điều nói dối vua. Trở lại cung vua, nhân lúc vua đi vắng, Cám bắt chim làm thịt nấu ăn rồi vứt lông chim ở ngoài vườn.\n" +
            "Lông chim vàng anh chôn ở vườn hoá ra hai cây xoan đào. Khi vua đi chơi vườn ngự, cành lá của chúng sà xuống che kín thành bóng, như hai cái lọng. Vua thấy cây đẹp rợp bóng, sai lính hầu mắc vọng vào hai cây rồi nằm chơi hóng mát. Khi vua đi khỏi thì cành cây lại vươn thẳng trở lại. Từ đó, không ngày nào Vua không ra nằm hóng mát ở hai cây xoan đào.\n"+
            "Cám biết chuyện ấy lại về nhà mách mẹ. Mẹ nó bảo, cứ sai thợ chặt cây làm khung cửi rồi kiếm điều nói dối vua. Về đến cung, nhân một hôm gió bão, Cám sai thợ chặt cây xoan đào lấy gỗ đóng khung cửi. Thấy cây bị chặt, vua hỏi thì Cám đáp:\n" +
            "- Cây bị đổ vì bão, thiếp sai thợ chặt làm khung cửi để dệt áo cho bệ hạ.\n" +
            "Nhưng khi khung cửi đóng xong. Cám ngồi vào dệt lúc nào cũng nghe thấy tiếng khung cửi rủa mình :\n" +
            "   Cót ca cót két\n" +
            "\n" +
            "   Lấy tranh chồng chị.\n" +
            "\n" +
            "   Chị khoét mắt ra\n" +
            "Thấy vậy Cám sợ hãi, vội về nhà mách mẹ. Mẹ nó bảo đốt quách khung cửi, rồi đem tro đi đổ cho rõ xa để được yên tâm. Về đến cung, Cám làm như lời mẹ nói. Nó đem tro đã đốt đi đổ ở lề đường cách xa hoàng cung.\n" +
            "Đống tro bên đường lại mọc lên một cây thị cao lớn, cành lá xum xuê. Đến mùa có quả, cây thị chỉ đậu được có một quả, nhưng mùi thơm ngát tỏa ra khắp nơi. Một bà lão hàng nước gần đó có một hôm đi qua dưới gốc, ngửi thấy mùi thơm, ngẩng đầu nhìn lên thấy quả thị trên cành cao, bèn giơ bị ra nói lẩm bẩm: \n" +
            "- Thị ơi thị à, rụng vào bị bà, bà để bà ngửi chứ bà không ăn. \n" +
            "Bà lão nói vừa dứt lời, thì quả thị rụng ngay xuống đúng vào bị. Bà lão nâng niu đem về nhà cất trong buồng, thỉng thoảng lại vào ngắm nghía và ngửi mùi thơm.\n" +
            "Ngày nào bà lão cũng đi chợ vắng. Từ trong quả thị chui ra một cô gái thân hình bé nhỏ như ngón tay, nhưng chỉ trong chớp mắt đã biến thành Tấm. Tấm vừa bước ra đã cầm lấy chổi quét dọn nhà cửa sạch sẽ, rồi đi vo gạo thổi cơm, hái rau ở vườn nấu canh giúp bà hàng nước. Đoạn Tấm lại thu hình bé nhỏ như cũ rồi chui vào quả thị. Lần nào đi chợ về, bà lão cũng thấy nhà cửa ngăn nắp, cơm ngon, canh ngọt sẵn sàng, thì lấy làm lạ.\n" +
            "Một hôm bà hàng nước giả vờ đi chợ, đến nửa đường lại lén trở về, rình ở bụi cây sau nhà. Trong khi đó, Tấm từ quả thị chui ra rồi cũng làm việc như mọi lần. Bà lão rón rén lại nhìn vào khe cửa. Khi thấy cô gái xinh đẹp thì bà mừng quá, bất thình lình xô cửa vào ôm choàng lấy Tấm, đoạn xé vụn vỏ thị.\n"+
            "Từ đó Tấm ở với bà hàng nước, hai người thương nhau như hai mẹ con. Hàng ngày Tấm giúp bà lão các việc thổi cơm, nấu nước, gói bánh, têm trầu để cho bà bán hàng.\n" +
            "Một hôm vua đi chơi ra khỏi hoàng cung, Thấy có quán nước bên đường sạch sẽ, bèn ghé vào. Bà lão mang trầu nước dâng lên vua. Thấy trầu têm cánh phượng, vua sực nhớ tới trầu vợ mình têm ngày trước cũng y như vậy, liền hỏi :\n" +
            "- Trầu này ai têm?\n" +
            "- Trầu này con gái lão têm - bà lão đáp.\n" +
            "- Con gái của bà đâu, gọi ra đây cho ta xem mặt.\n" +
            "Bà lão gọi Tấm ra. Tấm vừa xuất hiện, vua nhận ra ngay vợ mình ngày trước, có phần trẻ đẹp hơn xưa. Vua mừng quá, bảo bà lão hàng nước kể lại sự tình, rồi truyền cho quân hầu đưa kiệu rước Tấm về cung.\n" +
            "Cám Thấy Tấm trở về và được vua yêu thương như xưa, thì không khỏi ghen tỵ. Một hôm, Cám hỏi chị :\n" +
            "- Chị Tấm ơi, chị Tấm! Chị làm thế nào mà đẹp thế ?\n" +
            "Tấm không đáp, chỉ hỏi lại:\n" +
            "- Có muốn đẹp không để chị giúp !\n" +
            "Cám bằng lòng ngay. Tấm sai quân hầu đào một cái hố sâu và đun một nồi nước sôi. Tấm bảo Cám xuống hố rồi sai quân hầu dội nước sôi vào hố. Cám chết. Tấm sai đem xác làm mắm bỏ vào chĩnh gửi cho mụ dì ghẻ, nói là quà của con gái mụ gửi biếu. Mẹ Cám tưởng thật, lấy mắm ra ăn, bữa nào cũng nức nở khen ngon. Một con quạ ở đâu bay đến đậu trên nóc nhà kêu rằng:\n" +
            "- Ngon ngỏn ngòn ngon ! Mẹ ăn thịt con, có còn xin miếng.\n" +
            "Mẹ Cám giận lắm, chửi mắng ầm ĩ rồi vác sào đuổi quạ. Nhưng đến ngày ăn gần hết, dòm vào chĩnh, mụ thấy đầu lâu của con thì kinh hoàng lăn đùng ra chết','https://cdn.chanhtuoi.com/viectainha/2021/02/w800/w400/truyen-tranh-tam-cam.jpg.webp',1)";
    private String SQLQuery10 = "INSERT INTO truyen VALUES (null,'Tranh thiên hạ','Kết cục nửa ván cờ này đều chờ bọn họ đến hạ, quyết định thắng bại của chúng ta, mà cũng quyết định – quyền nắm thiên hạ!”\n"+
            "Nửa đêm, mưa thưa thớt rơi như những chấm nhỏ làm đẹp cho nền trời đen tối cùng với vầng trăng lạnh lẽo đang treo lơ lửng giữa khoảng không. Đỉnh núi Thương Mang cao nhất Đông triều như được phủ bởi một tầng lụa mỏng manh màu bạc dưới ánh trăng chiếu rọi, phảng phất như một viên ngọc bích đứng sừng sững trên bình nguyên hướng về bờ cõi đế vương, tôn quý, hùng vĩ mà thánh khiết, không thẹn cho danh xưng của chính nó “Vương sơn”!/n"+
            "Lúc này đây, có hai lão già ngồi tại trên đỉnh núi cao cao. Hai người đều chừng sáu mươi tuổi, tướng mạo thanh quắc [1], trong đôi mắt không chỉ lóe lên sự bình thản mà còn có hào quang trí tuệ. Một người mặc áo bào trắng, một người mặc áo bào đen, ngồi cách nhau một trượng[2], ở giữa là một khối cự thạch hình vuông, trên đỉnh của nó không biết bị vật gì mài đến ngay ngắn, bằng phẳng, trạm khắc thành một bàn cờ. Trên mặt bàn, những quân cờ được bố trí dày đặc, mỗi viên đều làm từ thạch đá kích thước đồng nhất./n"+
            "[1] Thanh quắc: Thanh trong thanh bạch. Từ thanh có nghĩa là sạch. Quắc trong quắc thước nghĩa là khỏe mạnh. Người già mà sức vóc tinh thần còn khỏe mạnh gọi là quắc thước./n"+
            "[2] 1 trượng = 3.33 mét/n"+
            "Ván cờ đã đi được nửa đường, hai bên thế lực ngang nhau, ai thắng ai thua còn chưa rõ./n"+
            "“Trời trong trăng sáng như vậy đã lâu không thấy!”. Lão già áo bào trắng ngồi bên trái ánh mắt trầm tư dời bàn cờ, ngẩng đầu nhìn trăng sao đầy trời, ngàn vạn lần cảm khái./n"+
            "“Loạn thế sao có bình yên, cũng thật khó có trong sáng!” Lão già mặc áo bào đen ngồi bên phải cũng dời mắt vào khoảng không “Giờ tý[3] đã qua, cái gì đến cũng nên đến đi.” Trong âm thanh lưu một chút chờ đợi./n"+
            "[3] Giờ tý: 11h đêm đến 1 h sáng/n"+
            "Lời nói của lão già vừa dứt, ánh sao tại màn trời bỗng nhiên trở nên rực rỡ, ở tận cùng trên cao dâng lên một ngôi sao sáng rỡ ngay lập tức xuyên thẳng chín tầng mây. Nhất thời, ánh sao che trọn, vượt qua cả vầng trăng sáng, trong nháy mặt chiếu rọi lên toàn bộ đất trời!/n"+
            "“Xuất hiện rồi! Xuất hiện rồi!”/n"+
            "Lão già mặc áo bào trắng ánh mắt sáng ngời chăm chú nhìn ngôi sao kia, vẻ lạnh nhạt bình tĩnh thường có trên mặt giờ đây không thể kiềm chế tia kích động./n"+
            "Ngay lúc này, một ngôi sao bỗng dưng mọc lên trên màn trời, cũng rạng rỡ vô biên, sáng rực mê hoặc ánh mắt, tựa như tại toàn thế gian này chỉ có thể chứa một mình ngôi sao ấy, kiêu ngạo vô song không ai bì nổi./n"+
            "“Xem đi! Cũng xuất hiện rồi! Cũng xuất hiện rồi đấy!”/n"+
            "Người mặc áo bào đen kích động đứng dậy, ngón tay chỉ lên ngôi sao trên bầu trời/n"+
            "“Rốt cục thì hai ngôi sao cũng đều xuất hiện!”/n"+
            "Lão già áo bào trắng cũng đứng dậy, nhìn lên hai ngôi sao rực rỡ bì kịp ánh trăng, chúng ở cách xa nhau mà độ sáng chói không hề thua kém!/n"+
            "“Rốt cục đã xuất hiện! Loạn thế đời này, rốt cục cũng cần kết thúc!” Lão già mặc áo bào đen tự lẩm bẩm kêu to, nhìn lên hai ngôi sao trên trời, vẻ mặt hiện lên sự hưng phấn sinh động lạ kì./n"+
            "“Loạn thế sẽ chấm dứt tại trong tay chúng nó, trên chín tầng trời chỉ có thể tồn tại một viên Vương Tinh! Hai ngôi sao đương thời gặp nhau, ai sẽ là người ngã xuống?” Lão già áo bào trắng nâng cao tay như muốn bao phủ hai ngôi sao phía chân trời. Trong thanh âm có kích động, cũng tồn tại nghi ngờ cùng hy vọng đối với tương lai không thể nắm giữ./n"+
            "Hai ngôi sao đang lóe sáng trên bầu trời bỗng nhiên dần dần thu lại ánh hào quang, tuy không còn chói mắt như lúc vừa rồi, nhưng vẫn sáng ngời so với những ngôi sao quanh nó./n"+
            "“Hai ngôi sao đương thời gặp nhau, ai sẽ là người ngã xuống! Việc ấy tùy vận mệnh định đoạt đi!” Lão già áo bào đen lòng đã bớt kích động, ánh mắt nhìn về phía vì sao nơi chân trời, thanh âm phảng phất như từ xa xưa truyền đến, ngân nga mà thâm trầm./n"+
            "“Vận mệnh sao?” Ánh mắt của lão già mặc áo bào trắng quyến luyến nhìn lên hai ngôi sao trên bầu trời, ẩn giấu chút tiếc hận cùng buồn bã./n"+
            "“Bàn cờ này liệu có nên hoàn thành luôn?” Lão già áo bào đen thu hồi ánh mắt, hướng về ván cờ đặt trước người./n"+
            "“Không được.” Lão già áo bào trắng liếc mắt về phía bàn cờ, sau đó lấy tay chỉ lên không trung “Bàn cờ này phải để bọn họ đến hạ.”/n"+
            "“Là bọn họ sao?” Lão già áo đen nhìn lại ván cờ rồi lại nhìn lên không trung, thoáng cười “Cũng tốt, để lại cho họ đến hạ vậy.”/n"+
            "“Chúng ta xuống núi đi, cũng là lúc ta với ông nên đi tìm người rồi.” Lão già áo bào trắng lần cuối liếc về phía ngôi sao trên bầu trời rồi quay người chuẩn bị xuống núi./n"+
            "“Thời điểm sau khi tìm được người, liệu có phải thắng bại của chúng đồng nghĩa với thắng bại của hai ta?” Lão già áo bào đen ánh mắt bình thản chợt bắn ra tia sắc bén./n"+
            "“Còn phải nói sao? Chúng ta tranh chấp hơn mười năm nay, thắng bại chưa phân. Kết cục nửa ván cờ này đều chờ bọn họ đến hạ, quyết định thắng bại của chúng ta, mà cũng quyết định – quyền nắm thiên hạ!” Lão già áo bào trắng quay đầu lại, vừa nhìn vừa cười với lão già áo bào đen, tuy cười vân đạm phong khinh mà lại ẩn chứa thâm ý./n"+
            "“Được!” Lão già áo bào đen gật đầu/n"+
            "Hai người quay đi với tư thế hiên ngang, chỉ để lại trên đỉnh núi Thương Mang một ván cờ dang dở./n"+
            "Về sau có người đi lên núi Thương Mang nhìn thấy ván cờ đó liền cảm thấy kinh ngạc không thôi, nhưng cũng không ai di động nó. Người có thể đi lên đỉnh núi cao nhất Đông Triều cũng không nhiều lắm, mà phàm là người đã lên tới đỉnh núi thì cũng không phải hạng người phàm tục. Đã có người lưu lại tàn cục, tức còn có người có thể đến hạ cờ hoàn thành./n"+
            "Rất nhiều năm sau, có hai người đi theo quỹ đạo vận mệnh, rốt cục cũng gặp gỡ nhau trên đỉnh Thương Mang, đối mặt với ván cờ mà vận mệnh để lại./n"+
            "Thời điểm này là thời của Kì đế đã nhiều năm./n"+
            "Đông Triều từ Thủy đế lập quốc truyền tới tay Kì đế đã qua hơn ba trăm năm. Thủy đế tài mưu kiệt xuất, võ công cái thế vô địch thiên hạ, đánh Đông dẹp Bắc, thảo phạt gian địch an ổn dân tâm, tạo nên nguồn gốc của Đông Triều đế quốc rộng lớn./n"+
            "Sau khi đế quốc được thành lập, Thủy đế xét công khen thưởng, phong bảy vị thuộc cấp có công trạng hiển hách nhất thành vương, phân chia thuộc địa, lấy họ đặt thành tên nước, chia thành bảy nước Hoàng, Ninh, (Hắc) Phong[4], Bạch, Hoa, (Bạch) Phong[5], Nam. Ông lấy quặng sắt đen nơi đáy biển Bắc Hải đúc thành tám tấm huyền lệnh. Tấm lớn nhất đặt danh Huyền Tôn Lệnh đưa Đế nắm giữ, bảy tấm còn lại ngắn hơn đặt danh Huyền Mặc Lệnh, phân cho người đứng đầu bảy nước. Lúc phân chia này cũng là lúc Đế cùng thất Vương lấy máu ăn thề: Huyền Tôn Lệnh xuất ra, bảy nước cúi đầu!/n"+
            "[4] Phong 丰: trong phong thịnh, phong phú. Từ nay nước Phong này được gọi là Hắc Phong quốc/n"+
            "[5] Phong 风:  gió. Từ nay nước Phong này được gọi là Bạch Phong quốc/n"+
            "Sau Thủy đế, Thành đế, Quan đế, Ngôn đế đều là các thế hệ minh chủ, thu nạp người tài có đức, thể nghiệm quan sát dân tình, giảm lao dịch bớt sưu thuế, đường lối chính trị sáng suốt, các nước chư hầu giữ vững bổn phận, trung thành tận tâm với hoàng đế.  Đông Triều trong tay các vị Đế này càng ngày càng trở nên cường đại và hưng thịnh./n"+
            "Giữa thời truyền tới tay Chí đế, Ích đế, Tề đế, Triệu đế, những vị hoàng đế này đều mười phần không tài cán, chỉ giữ gìn những cái đã có thôi âu cũng là điều khó. Tới Gia đế, Hỉ đế, Di đế, những người này thật chỉ giỏi việc hoang phí vô độ, ham thích an nhàn hường lạc, lơ đi chính sự, để triều chính rơi vào tay nhóm gian thần nịnh hót./n"+
            "Về sau tới tay Lễ đế, ưa khoe khoang thể diện, thích chưng diện xa hoa, mỗi lần đi tuần đều yêu cầu tới hành cung xa hoa, hao tài tốn của. Hai lần huy động quân xuất chinh chiến Mông thành đều đại bại mà về, làm cho dân chúng trong nước lầm than, thanh âm oán hận nổi lên khắp bốn phương. Cùng lúc, các nước chư hầu cũng dần dần không thần phục. Đông Triều Đế quốc cường đại một thế nay ngày càng lụn bại. Đầu tiên là Ninh vương của Ninh quốc huy động quân đội nổi dậy, ý đồ đảo chính, muốn đuổi giết tới tận Đế đô. Mà Lễ đế cũng không đợi Ninh quân đuổi tới Kim Loan điện, thân thể sớm bị tửu sắc ăn mòn vì hoảng sợ quá độ mà qua đời tại Trì Long cung xa hoa./n"+
        "Cảnh thái tử thế chỗ hiệu Cảnh đế. Cảnh đế phát ra Huyền Tôn Lệnh, hiệu lệnh sáu nước chư hầu, chỉ huy quân cần vương tập hợp từ đại quân sáu nước, đẩy lui Ninh quân. Ninh vương bại trận mà chôn thân nơi chiến trận, nay đất phong vào tay tam đại quốc gia Bạch Phong, Hoàng, Hắc Phong lân cận. ','https://static.8cache.com/cover/eJzLyTDWz8qKdyr28gowyHQLtSyuKHD2MHB3jyh29vQxM8wtMArWdc3IzkzNcsmJ98uOyjbwsXQPLktNiS9IyTDzLTa1tEjJMiiNiHc1ikh0Mfe3zIzKcrQtNzI01c0wNjICAPEGHpI=/tranh-thien-ha.jpg',1)";
    private String SQLQuery11 = "INSERT INTO truyen VALUES (null,'Cây tre trăm đốt','Ngày xưa, có một ông già nhà quê có một cô gái đẹp. Trong nhà phải thuê một đầy tớ trai, ông ta muốn lợi dụng nó làm việc khỏi trả tiền, mới bảo nó rằng: “Mày chịu khó làm ăn với tao rồi tao gả con gái cho”. Người ở mừng lắm, ra sức làm lụng tới khuya không nề hà mệt nhọc. Nó giúp việc được ba năm, nhà ông ta mỗi ngày một giàu có.\n"+
            "Ông nhà giàu không còn nghĩ đến lời hứa cũ nữa, đem con gái gả cho con một nhà phú hộ khác ở trong làng./n"+
            "Sáng hôm sắp đưa dâu, ông chủ gọi đứa ở lên lừa nó một lần nữa, bảo rằng: “Bây giờ mày lên rừng tìm cho ra một cây tre có trăm đốt đem về đây làm đũa ăn cưới, thì tao cho mày lấy con gái tao ngay”./n"+
            "Đứa ở tưởng thật, vác dao đi rừng. Nó kiếm khắp nơi, hết rừng này qua rừng nọ, không tìm đâu thấy có cây tre đủ trăm đốt. Buồn khổ quá, nó ngồi một chỗ ôm mặt khóc. Bỗng thấy có một ông lão râu tóc bạc phơ, tay cầm gậy trúc hiện ra bảo nó: “Tại sao con khóc, hãy nói ta nghe, ta sẽ giúp cho”. Nó bèn đem đầu đuôi câu chuyện ông phú hộ hứa gả con gái cho mà kể lại. Ông lão nghe xong, mới bảo rằng: “Con đi chặt đếm đủ trăm cái đốt tre rồi đem lại đây ta bảo”./n"+
            "Nó làm theo y lời dặn, ông dạy nó đọc: “Khắc nhập, khắc nhập” (vào ngay, vào ngay) đủ ba lần, thì một trăm khúc tre tự nhiên dính lại với nhau thành một cây trẻ đủ một trăm đốt. Nó mừng quá, định vác về, nhưng cây tre dài quá, vướng không đi được. Ông lão bảo nó đọc: “Khắc xuất, khắc xuất” (ra ngay, ra ngay) đúng ba lần thì cây tre trăm đốt lại rời ra ngay từng khúc./n"+
            "Nó bèn bó cả lại mà gánh về nhà. Đến nơi thấy hai họ đang ăn uống vui vẻ, sắp đến lúc rước dâu, nó mới hay là ông chủ đã lừa nó đem gả con gái cho người ta rồi. Nó không nói gì, đợi lúc nhà trai đốt pháo cưới, bèn đem một trăm khúc tre xếp dài dưới đất, rồi lẩm bẩm đọc: “Khắc nhập, khắc nhập” cho liền lại thành một cây tre trăm đốt, đoạn gọi ông chủ đến bảo là đã tìm ra được, và đòi gả con gái cho nó. Ông chủ lấy làm lạ cầm cây tre lên xem, nó đọc luôn: “Khắc nhập, khắc nhập”, thì ông ta bị dính liền ngay vào cây tre, không làm sao gỡ ra được. Ông thông gia thấy vậy chạy đến, định gỡ cho, nó lại đọc luôn: “Khắc nhập, khắc nhập”, thì cả ông cũng bị dính theo luôn, không lôi ra được nữa./n"+
            "Hai họ thấy thế không còn ai dám lại gần nó nữa. Còn hai ông kia không còn biết làm thế nào đành van lạy xin nó thả ra cho. Ông chủ hứa gả con gái cho nó, ông thông gia xin về nhà ngay, nó để cho cả hai thề một hồi rồi nó mới đọc: “Khắc xuất, khắc xuất” thì hai ông rời ngay cây tre, và cây tre cũng rời ra trăm khúc./n"+
            "Mọi người đều lấy làm khiếp phục đứa ở, ông chủ vội gả con gái cho nó, và từ đó không còn dám khinh thường nó nữa. ','https://cf.shopee.vn/file/3122d8267ee5fc21a19231e65e031b4f',1)";

    // tạo bảng tại phương thức
    public databasedoctruyen(@Nullable Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //Thực hiện các câu tri vấn không trả về kết quả
        db.execSQL(SQLQuery);
        db.execSQL(SQLQuery1);
        db.execSQL(SQLQuery2);
        db.execSQL(SQLQuery3);
        db.execSQL(SQLQuery4);
        db.execSQL(SQLQuery5);
        db.execSQL(SQLQuery6);
        db.execSQL(SQLQuery7);
        db.execSQL(SQLQuery8);
        db.execSQL(SQLQuery9);
        db.execSQL(SQLQuery10);
        db.execSQL(SQLQuery11);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    //Phương thức lấy tất cả tài khoản
    public Cursor getData(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM "+ TABLE_TAIKHOAN,null);
        return res;
    }
    // phương thức add tài khoản vào database
    public void AddTaiKhoan(TaiKhoan taiKhoan){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // Thực hiện Insert thông qua ContenValuse
        values.put(TEN_TAI_KHOAN, taiKhoan.getmTenTaiKhoan());
        values.put(MAT_KHAU,taiKhoan.getmMatKhau());
        values.put(EMAIL,taiKhoan.getmEmail());
        values.put(PHAN_QUYEN,taiKhoan.getmPhanQuyen());
        db.insert(TABLE_TAIKHOAN,null ,values);
        // Đóng lại khi không dùng
        db.close();
        Log.e("ADD TK","TC");
    }
    //Lấy 3 tin tức mới nhất
    public Cursor getData1(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT* FROM "+TABLE_TRUYEN+" ORDER BY "+ID_TRUYEN+" DESC LIMIT 3",null);
        return res;
    }
    //Lấy tất cả các truyện
    public Cursor getdata2(){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res = db.rawQuery("SELECT * FROM "+TABLE_TRUYEN,null);
        return res;
    }
    // add truyện
    public void AddTruyen(Truyen truyen){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TEN_TRUYEN,truyen.getTenTruyen());
        values.put(NOI_DUNG,truyen.getNoiDung());
        values.put(IMAGE,truyen.getAnh());
        values.put(ID_TAI_KHOAN,truyen.getID_TK());
        db.insert(TABLE_TRUYEN,null, values);
        db.close();
    }
    public int Delete(int i){
        SQLiteDatabase db = this.getReadableDatabase();
        int res = db.delete(TABLE_TRUYEN,ID_TRUYEN+" = "+i,null);
        return res;
    }
}