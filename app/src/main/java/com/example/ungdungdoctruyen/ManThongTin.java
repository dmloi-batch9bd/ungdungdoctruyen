package com.example.ungdungdoctruyen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class ManThongTin extends AppCompatActivity {
    TextView txtThongTinapp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_thong_tin);
        txtThongTinapp = (TextView)findViewById(R.id.textviewthongtin);
        String thongtin = "Ứng dụng được phát triển bởi \n'Dương Minh Lợi & Đặng Phước Triều'\n01/06/2021";
        txtThongTinapp.setText(thongtin);
    }
}