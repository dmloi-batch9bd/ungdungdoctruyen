package com.example.ungdungdoctruyen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ungdungdoctruyen.database.databasedoctruyen;
import com.example.ungdungdoctruyen.model.Truyen;

public class ManDangBai extends AppCompatActivity {
    EditText editTenTruyen, editNoiDung,editAnh;
    Button btDangBai;
    databasedoctruyen databasedoctruyen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_dang_bai);
        anhxa();
        databasedoctruyen =new databasedoctruyen(this);
        btDangBai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tentruyen = editTenTruyen.getText().toString();
                String noidung = editNoiDung.getText().toString();
                String img = editAnh.getText().toString();
                Truyen truyen = CreatTruyen();
                if(tentruyen.equals("")|| noidung.equals("")|| img.equals("")){
                    Toast.makeText(ManDangBai.this,"Yêu cầu nhập đầy đủ thông tin",Toast.LENGTH_SHORT).show();
                //    Log.e("ERR: ","Nhập đầy đủ thông tin");
                }
                else {
                    databasedoctruyen.AddTruyen(truyen);
                    Intent intent = new Intent(ManDangBai.this,ManAdmin.class);
                    finish();
                    startActivity(intent);
                }
            }
        });

    }
    //Phương thức tạo truyện
    private Truyen CreatTruyen(){
        String tentruyen = editTenTruyen.getText().toString();
        String noidung = editNoiDung.getText().toString();
        String img = editAnh.getText().toString();
        Intent intent = getIntent();
        int id = intent.getIntExtra("Id",0);
        Truyen truyen= new Truyen(tentruyen,noidung,img,id);
        return truyen;
    }
    private void anhxa(){
        editTenTruyen = (EditText)findViewById(R.id.dbTenTruyen);
        editNoiDung = (EditText)findViewById(R.id.dbNoiDungn);
        editAnh = (EditText)findViewById(R.id.dbimg);
        btDangBai = (Button)findViewById(R.id.dbDangBai);
    }
}