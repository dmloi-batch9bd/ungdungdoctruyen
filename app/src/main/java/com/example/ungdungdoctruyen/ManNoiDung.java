package com.example.ungdungdoctruyen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ManNoiDung extends AppCompatActivity {
    TextView txtTenTruyen, txtNoiDung;
    Button buttonQuayVe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_noi_dung);
        txtNoiDung = (TextView)findViewById(R.id.noidung);
        txtTenTruyen = (TextView)findViewById(R.id.TenTruyen);
        buttonQuayVe = (Button)findViewById(R.id.buttonQuayve) ;
        //Lấy dữ liệu
        Intent intent = getIntent();
        String tentruyen = intent.getStringExtra("tentruyen");
        String noidung = intent.getStringExtra("noidung");
        txtTenTruyen.setText(tentruyen);
        txtNoiDung.setText(noidung);
        //cho phép cuộn nội dung khi xem
        txtNoiDung.setMovementMethod(new ScrollingMovementMethod());

        buttonQuayVe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}