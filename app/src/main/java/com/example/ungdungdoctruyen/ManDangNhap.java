package com.example.ungdungdoctruyen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ungdungdoctruyen.database.databasedoctruyen;

public class ManDangNhap extends AppCompatActivity {
    EditText editTaiKhoan, editMatKhau;
    Button buttonDangNhap;
    TextView txtdangky, txtquenmk;
    //tạo đối tượng database
    databasedoctruyen databasedoctruyen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_dang_nhap);
        anhxa();
        databasedoctruyen = new databasedoctruyen(this);
        txtdangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ManDangNhap.this,ManDangKy.class);
                startActivity(intent);
            }
        });
        buttonDangNhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //gán các biến là các giá trị nhập vào
                String tentaikhoan = editTaiKhoan.getText().toString();
                String matkhau = editMatKhau.getText().toString();
                if (tentaikhoan.equals("")||matkhau.equals("")){
                    Toast.makeText(ManDangNhap.this,"Chưa nhập đầy đủ thông tin",Toast.LENGTH_LONG).show();
                }
                else {
                    //sử dụng con trở lấy dữ liệu gọi getData để lấy hết dữ liệu tài khoản ở SQL
                    Cursor cursor = databasedoctruyen.getData();
                    // thực hiện vòng lập để lấy dữ liệu từ cursor
                    while (cursor.moveToNext()){
                        // lấy dữ liệu và gán vào biến, dữ liệu tài khoản ở ô 1 và mật khẩu ở ô 2, ô 0 là idtaikhoan, oo3 là email, ô 4 là phânquyen
                        String datatentaikhoan = cursor.getString(1);
                        String datamatkhau = cursor.getString(2);
                        if (datatentaikhoan.equals(tentaikhoan)&&datamatkhau.equals(matkhau)){
                            //lấy dữ liệu phân quyền và id
                            int phanquyen = cursor.getInt(4);
                            int idd = cursor.getInt(0);
                            String email = cursor.getString(3);
                            String tentk = cursor.getString(1);
                            //Chuyển qua màn hình mainActivity
                            Intent intent = new Intent(ManDangNhap.this,MainActivity.class);
                            //Gửi dữ liệu qua Activyty qua MainActivity
                            intent.putExtra("phanq",phanquyen);
                            intent.putExtra("idd",idd);
                            intent.putExtra("email",email);
                            intent.putExtra("tentaikhoan",tentk);
                            startActivity(intent);
                            Toast.makeText(ManDangNhap.this,"Dăng nhập thành công!",Toast.LENGTH_LONG).show();
                        }
                    }
                    //Thực hiện trả cursor về đầu
                    cursor.moveToFirst();
                    //Đóng cursor khi không dùng
                    cursor.close();;
                }
                }
        });
    }
    private void anhxa(){
        editTaiKhoan = (EditText)findViewById(R.id.editTaikhoan);
        editMatKhau = (EditText)findViewById(R.id.editMatkhau);
        buttonDangNhap = (Button)findViewById(R.id.buttonDangnhap);
        txtdangky = (TextView)findViewById(R.id.txtdangky);
        txtquenmk = (TextView)findViewById(R.id.txtquenmk);
    }
}