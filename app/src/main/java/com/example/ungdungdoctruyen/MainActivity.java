package com.example.ungdungdoctruyen;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.widget.Toolbar;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.example.ungdungdoctruyen.adapter.adapterDanhSachTruyen;
import com.example.ungdungdoctruyen.adapter.adapterThongTin;
import com.example.ungdungdoctruyen.adapter.adapterTruyen;
import com.example.ungdungdoctruyen.adapter.adapterchuyenmuc;
import com.example.ungdungdoctruyen.database.databasedoctruyen;
import com.example.ungdungdoctruyen.model.TaiKhoan;
import com.example.ungdungdoctruyen.model.Truyen;
import com.example.ungdungdoctruyen.model.chuyenmuc;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    ViewFlipper viewFlipper;
    NavigationView navigationView;
    ListView listView, listViewNew, listViewThongTin;
    GridView gridViewTruyen;
    DrawerLayout drawerLayout;
    int [] arrayHinh = {R.drawable.anh1,R.drawable.anh2,R.drawable.anh3};

    String email;
    String tentaikhoan;

    ArrayList<Truyen> TruyenArrayList;
    adapterTruyen adapterTruyen;
    databasedoctruyen databasedoctruyen;
    adapterDanhSachTruyen adapterDanhSachTruyen;

    ArrayList<chuyenmuc> chuyenmucArrayList;
    ArrayList<TaiKhoan> taiKhoanArrayList;

    adapterchuyenmuc adapterchuyenmuc;
    adapterThongTin adapterThongTin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        databasedoctruyen = new databasedoctruyen(this);
        //nhận dữ liệu đăng nhập và đưa vào
        Intent intentpq = getIntent();
        int i = intentpq.getIntExtra("phanq",0);
        int idd = intentpq.getIntExtra("idd",0);
        email = intentpq.getStringExtra("email");
        tentaikhoan = intentpq.getStringExtra("tentaikhoan");

        anhxa();
        ActionViewFlipper();
        ActionBar();
        danhsachtruyen();
        gridViewTruyen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this,ManNoiDung.class);
                String tent = TruyenArrayList.get(position).getTenTruyen();
                String noidungt = TruyenArrayList.get(position).getNoiDung();

                intent.putExtra("tentruyen",tent);
                intent.putExtra("noidung",noidungt);
                startActivity(intent);
            }
        });
        // sự kiện cho lick vào truyện mới
        listViewNew.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this,ManNoiDung.class);
                String tent = TruyenArrayList.get(position).getTenTruyen();
                String noidungt = TruyenArrayList.get(position).getNoiDung();

                intent.putExtra("tentruyen",tent);
                intent.putExtra("noidung",noidungt);
                startActivity(intent);
            }
        });
        //click item cho listview của thanh menu
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // đăng bài.
                if(position == 0){
                    if(i == 2){
                        Intent intent = new Intent(MainActivity.this,ManAdmin.class);
                        //gửi id tai khoan qua admin
                        intent.putExtra("Id",idd);
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(MainActivity.this,"Bạn không có quyền đăng bài",Toast.LENGTH_SHORT).show();
                        Log.e("Đăng bài:","Bạn không có quyền ");
                    }
                }
                //vị trí ấn là thông tin sẽ chuyển đến activy man thong tin
                else if(position == 1){
                    Intent intent = new Intent(MainActivity.this,ManThongTin.class);
                    startActivity(intent);
                }
                // đăng xuất
                else  if(position == 2){
                    finish();
                }
            }
        });
        //sự kiện lick cho gridiewdanhsach
        gridViewTruyen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this,ManNoiDung.class);
                String tent = TruyenArrayList.get(position).getTenTruyen();
                String noidungt = TruyenArrayList.get(position).getNoiDung();

                intent.putExtra("tentruyen",tent);
                intent.putExtra("noidung",noidungt);
                startActivity(intent);
            }
        });
    }

// đưa dữ liệu vào gridview
    private void danhsachtruyen() {
        TruyenArrayList = new ArrayList<>();
        Cursor cursor3 = databasedoctruyen.getdata2();
        while (cursor3.moveToNext()){
            int id = cursor3.getInt(0);
            String tentruyen = cursor3.getString(1);
            String noidung = cursor3.getString(2);
            String anh = cursor3.getString(3);
            int id_tk =cursor3.getInt(4);
            TruyenArrayList.add(new Truyen(id,tentruyen,noidung,anh,id_tk));
            adapterDanhSachTruyen = new adapterDanhSachTruyen(getApplicationContext(),TruyenArrayList);
            gridViewTruyen.setAdapter(adapterDanhSachTruyen);
        }
        cursor3.moveToFirst();
        cursor3.close();
    }


    //Thanh acctionBar
    private void ActionBar() {
        // hàm hỗ trợ toolbar
        setSupportActionBar(toolbar);
        //set nút cho toolbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        // tạo icom cho toolbar
        toolbar.setNavigationIcon(android.R.drawable.ic_menu_sort_by_size);
        //Bắt đầu sự kiên click
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(GravityCompat.START);
            }
        });

    }
//Hình quảng cáo tự động
    private void ActionViewFlipper() {
        for (int i=0; i<arrayHinh.length;i++){
            ImageView imageView = new ImageView(this);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageResource(arrayHinh[i]);
            viewFlipper.addView(imageView);
        }
        viewFlipper.setFlipInterval(3000);
        viewFlipper.setAutoStart(true);
    }

    private void anhxa(){
        toolbar = (Toolbar)findViewById(R.id.toolbarmanhinhchinh);
        viewFlipper = (ViewFlipper)findViewById(R.id.viewFlipper);
        listViewNew = (ListView)findViewById(R.id.listviewNew);
        listView = (ListView)findViewById(R.id.listviewmanhinhchinh);
        listViewThongTin = (ListView)findViewById(R.id.listviewwthongtin);
        navigationView = (NavigationView)findViewById(R.id.navigationView);
        drawerLayout = (DrawerLayout)findViewById(R.id.drawerlayout);
        gridViewTruyen = (GridView) findViewById(R.id.gridTruyen);
        // đưa dữ liệu vào list view
        TruyenArrayList = new ArrayList<>();
        Cursor cursor1 = databasedoctruyen.getData1();
        while (cursor1.moveToNext()){
            int id = cursor1.getInt(0);
            String tentruyen = cursor1.getString(1);
            String noidung = cursor1.getString(2);
            String anh = cursor1.getString(3);
            int id_tk = cursor1.getInt(4);

            TruyenArrayList.add(new Truyen(id, tentruyen, noidung,anh,id_tk));
            adapterTruyen = new adapterTruyen(getApplicationContext(),TruyenArrayList);
            listViewNew.setAdapter(adapterTruyen);
        }
        cursor1.moveToFirst();
        cursor1.close();

        //listview thông tin thanh navigo khi dang nhap vào
        taiKhoanArrayList = new ArrayList<>();
        taiKhoanArrayList.add(new TaiKhoan(tentaikhoan,email));

        adapterThongTin = new adapterThongTin(this, R.layout.navigation_thongtin,taiKhoanArrayList);
        listViewThongTin.setAdapter(adapterThongTin);
        //Chuyên mục
        chuyenmucArrayList = new ArrayList<>();
        chuyenmucArrayList.add(new chuyenmuc( "Đăng bài", "https://static.thenounproject.com/png/204040-200.png"));
        chuyenmucArrayList.add(new chuyenmuc("Thông tin","https://static.thenounproject.com/png/476622-200.png"));
        chuyenmucArrayList.add(new chuyenmuc("Đăng xuất","https://image.freepik.com/free-icon/login-symbol_318-9896.jpg"));
        adapterchuyenmuc = new adapterchuyenmuc(getApplicationContext(),chuyenmucArrayList);
        listView.setAdapter(adapterchuyenmuc);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.mymenu,menu);
        return true;
    }
// sự kiện lich vào của tìm kiếm search
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu1:
                Intent intent = new Intent(MainActivity.this,ManTimKiem.class);
                startActivity(intent);
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}