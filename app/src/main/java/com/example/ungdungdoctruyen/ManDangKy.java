package com.example.ungdungdoctruyen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.ungdungdoctruyen.database.databasedoctruyen;
import com.example.ungdungdoctruyen.model.TaiKhoan;

public class ManDangKy extends AppCompatActivity {
    EditText editDKTaiKhoan, editDKMatKhau,editDKEmail;
    Button buttonDangKy, buttonQuayVe;
    databasedoctruyen databasedoctruyen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_man_dang_ky);
        databasedoctruyen = new databasedoctruyen(this);
        anhxa();
        buttonDangKy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String taikhoan = editDKTaiKhoan.getText().toString();
                String matkhau = editDKMatKhau.getText().toString();
                String email = editDKEmail.getText().toString();
                TaiKhoan taiKhoan1 = CreateTaiKhoan();
                if (taikhoan.equals("")||matkhau.equals("")||email.equals("")){
                    Toast.makeText(ManDangKy.this,"Chưa nhập đầy đủ thông tin",Toast.LENGTH_LONG).show();
                }
                else {
                    databasedoctruyen.AddTaiKhoan(taiKhoan1);
                    Toast.makeText(ManDangKy.this,"Đăng ký thành công",Toast.LENGTH_LONG).show();
                }
            }
        });
        buttonQuayVe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    //Phương thức tạo tài khoản
    private TaiKhoan CreateTaiKhoan(){
        String taikhoan = editDKTaiKhoan.getText().toString();
        String matkhau = editDKMatKhau.getText().toString();
        String email = editDKEmail.getText().toString();
        int phanquyen = 1;
        TaiKhoan tk = new TaiKhoan(taikhoan, matkhau, email, phanquyen);
        return tk;
    }
    private void anhxa(){
        editDKTaiKhoan = (EditText)findViewById(R.id.editDKTaiKhoan);
        editDKMatKhau = (EditText)findViewById(R.id.editDKMatKhau);
        editDKEmail = (EditText)findViewById(R.id.editDKEmail);
        buttonDangKy = (Button)findViewById(R.id.buttonDangky);
        buttonQuayVe = (Button)findViewById(R.id.buttonQuayve);
    }
}